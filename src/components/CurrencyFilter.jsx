import { useContext } from "react";
import { ThemeContext } from "../App";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

export default function CurrencyFilter({ countryData, setCurrency }) {
  const { dark } = useContext(ThemeContext);

  function handleSelect(e) {
    setCurrency(e.target.value);
  }

  let currencies = countryData.reduce((accumulator, country) => {
    if (country.currencies) {
      let currencyArray = Object.values(country.currencies);
      currencyArray.map((currency) => {
        if (!accumulator.includes(currency)) {
          accumulator.push(currency.name);
        }
      });
    }
    return accumulator;
  }, []);

  let crr = [...new Set(currencies)];

  return (
    <>
      <div className="filter">
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Currency Filter</InputLabel>
          <Select labelId="demo-simple-select-label" onChange={handleSelect}>
            <MenuItem value="">Currency Filter</MenuItem>
            {crr.map((currency, index) => {
              return (
                <MenuItem key={index} value={currency}>
                  {currency}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
    </>
  );
}
