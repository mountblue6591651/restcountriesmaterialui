import { useContext } from "react";
// import "../style/sort.css";
import { ThemeContext } from "../App";
import {
  FormControl,
  InputLabel,
  ListSubheader,
  Menu,
  MenuItem,
  Select,
} from "@mui/material";

export default function Sort({ changeSort }) {

  function handleChange(e) {
    changeSort(e.target.value);
  }

  return (
    <>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Sort</InputLabel>
        <Select onChange={handleChange}>
          <MenuItem value="">Sort</MenuItem>
          <ListSubheader>Population</ListSubheader>
          <MenuItem value="populationAsc">Ascending</MenuItem>
          <MenuItem value="populationDesc">Descending</MenuItem>
          <ListSubheader>Area</ListSubheader>
          <MenuItem value="areaAsc">Ascending</MenuItem>
          <MenuItem value="areaDesc">Descending</MenuItem>
        </Select>
      </FormControl>
    </>
  );
}
