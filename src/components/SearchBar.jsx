import React, { useContext } from "react";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import "../style/searchbar.css";
import { ThemeContext } from "../App";
import { Box, FormControl, Input } from "@mui/material";
import { Search } from "@mui/icons-material";

export default function SearchBar({ onSearch }) {
  function handleChange(event) {
    onSearch(event.target.value);
  }

  return (
    <Box sx={{ display: "flex", padding:1.3 }}>
      <Search />
      <FormControl>
        <Input
          type="text"
          placeholder="Search for a country..."
          onChange={handleChange}
          disableUnderline
        />
      </FormControl>
    </Box>
  );
}
