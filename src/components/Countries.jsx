import React, { useContext, useEffect, useState } from "react";
import SearchBar from "./SearchBar";
// import "../style/countries.css";
import Filter from "./Filter";
import { ThemeContext } from "../App";
import Sort from "./Sort";
import { Link } from "react-router-dom";
import { Rolling } from "react-loading-io";
import CurrencyFilter from "./CurrencyFilter";
import { Card, Grid, Paper, CardMedia } from "@mui/material";

import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default function Countries() {
  const [countryData, setCountryData] = useState([]);
  const [region, setRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [currency, setCurrency] = useState("");
  const [name, setName] = useState("");
  const [loader, setLoader] = useState(true);
  const [sort, setSort] = useState("");
  const [error, setError] = useState(false);

  const { dark } = useContext(ThemeContext);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch("https://restcountries.com/v3.1/all");
        const res2 = await res.json();
        const dataReturned = res2;
        setLoader(false);
        setCountryData(dataReturned);
      } catch (error) {
        setLoader(false);
        setError(true);
      }
    };

    fetchData();
  }, []);

  const filterCountries = [];

  countryData.forEach((country) => {
    let currencies = [];
    if (country.currencies) {
      let currencyArray = Object.values(country.currencies);
      currencies = currencyArray.map((curr) => {
        return curr.name;
      });
    }
    if (
      (!region || country.region === region) &&
      (!subRegion || country.subregion === subRegion) &&
      (!currency || currencies.includes(currency)) &&
      (!name || country.name.common.toLowerCase().includes(name.toLowerCase()))
    ) {
      filterCountries.push(country);
    }
  });
  if (sort) {
    if (sort.includes("population")) {
      filterCountries.sort((countryA, countryB) => {
        return countryA.population - countryB.population;
      });
    } else {
      filterCountries.sort((countryA, countryB) => {
        return countryA.area - countryB.area;
      });
    }
    if (sort.includes("Desc")) {
      filterCountries.reverse();
    }
  }

  return (
    <>
      <Grid container spacing={3} sx={{margin:0, display:'flex', flexWrap:'wrap'}} >
        <Grid item xs="3">
          <Paper>
            <SearchBar onSearch={setName} />
          </Paper>
        </Grid>
        <Grid item xs="2">
          <Paper>
            <Sort changeSort={setSort} />
          </Paper>
        </Grid>
        <Grid item xs="2">
          <Paper>
            <CurrencyFilter
              countryData={countryData}
              setCurrency={setCurrency}
            />
          </Paper>
        </Grid>
        <Grid item xs="2">
          <Paper>
            <Filter
              countryData={countryData}
              type={"region"}
              setRegion={setRegion}
              setSubRegion={setSubRegion}
              subRegion={subRegion}
              region={region}
            />
          </Paper>
        </Grid>
        <Grid item xs="2">
          <Paper>
            <Filter
              countryData={countryData}
              type={region}
              setRegion={setRegion}
              setSubRegion={setSubRegion}
              subRegion={subRegion}
              region={region}
            />
          </Paper>
        </Grid>
      </Grid>
      <Grid container spacing={3} p={5} justifyContent="space-around">
        {error && <p>Error</p>}
        {loader && <Rolling size={100} />}
        {!loader && !error && filterCountries.length === 0 && (
          <p>No countries found.</p>
        )}
        {!loader &&
          !error &&
          filterCountries.length > 0 &&
          filterCountries.map((country) => (
            <Grid item >
              <Link to={`country/${country.cca3}`} key={country.cca3}>
                <Card>
                  <CardMedia
                    component="img"
                    sx={{ width: "23rem", height: "13rem" }}
                    image={country.flags.png}
                    alt={country.flags.alt}
                    fit="scale-down"
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {country.name.common}
                    </Typography>
                    <Typography>
                      Population: <span>{country.population}</span>
                    </Typography>
                    <Typography>
                      Region: <span>{country.region}</span>
                    </Typography>
                    <Typography>
                      Capital:{" "}
                      <span>
                        {country.capital ? country.capital[0] : "No Capital"}
                      </span>
                    </Typography>
                    <Typography>
                      Currency:{" "}
                      <span>
                        {country.currencies
                          ? Object.keys(country.currencies)
                          : "No Currency"}
                      </span>
                    </Typography>
                  </CardContent>
                </Card>
              </Link>
            </Grid>
          ))}
      </Grid>
    </>
  );
}
