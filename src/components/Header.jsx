// import '../style/header.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-regular-svg-icons";
import { ThemeContext } from "../App";
import { useContext } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import DarkModeOutlinedIcon from "@mui/icons-material/DarkModeOutlined";
import { IconButton, Input } from "@mui/material";

export default function Header() {
  const { dark, setDark } = useContext(ThemeContext);
  function handleCheck(event) {
    if (event.target.checked) {
      console.log("Checked");
      setDark(true);
    } else {
      setDark(false);
    }
  }

  return (
    // <header className={dark ? 'dark' : null}>
    //     <h1>Where in the world?</h1>
    //     <input type="checkbox" name="darkmode" id="darkmode" onChange={handleCheck} hidden/>
    //     <label htmlFor="darkmode"><FontAwesomeIcon icon={faMoon} /> {dark ? 'Light Mode' : 'Dark Mode'}</label>
    // </header>

    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        position="sticky"
        style={{ backgroundColor: "white", color: "black" }}
      >
        <Toolbar>
          <Typography
            variant="h5"
            fontWeight={700}
            component="div"
            sx={{ flexGrow: 1 }}
          >
            Where in the world?
          </Typography>

          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <DarkModeOutlinedIcon/>

            <Button color="inherit">Dark Mode</Button>
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
