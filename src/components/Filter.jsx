import { useContext } from "react";
import { ThemeContext } from "../App";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
// import "../style/filter.css";

export default function Filter({
  countryData,
  type,
  setRegion,
  setSubRegion,
  subRegion,
  region,
}) {
  const { dark } = useContext(ThemeContext);

  function handleChange(e) {
    if (type === "region") {
      console.log("HIII");
      setSubRegion("");
      setRegion(e.target.value);
    } else {
      console.log("hey");
      setSubRegion(e.target.value);
    }
  }

  const regions = countryData.reduce((acc, country) => {
    if (type !== "region") {
      if (!acc.includes(country["subregion"]) && country["region"] === type) {
        acc.push(country["subregion"]);
      }
    } else {
      if (!acc.includes(country["region"])) {
        acc.push(country["region"]);
      }
    }
    return acc;
  }, []);

  return (
    <>
      <div className="filter">
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">
            Filter by {type === "region" ? "Region" : "Subregion"}
          </InputLabel>
          <Select onChange={handleChange}>
            <MenuItem value="">
              Filter by {type === "region" ? "Region" : "Subregion"}
            </MenuItem>
            {regions.map((region, index) => {
              return (
                <MenuItem key={index} value={region}>
                  {region}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
    </>
  );
}
