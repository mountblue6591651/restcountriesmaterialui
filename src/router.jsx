import { createBrowserRouter } from "react-router-dom";
import Countries from "./components/Countries";
import Details from "./components/Detail";
import ErrorPage from "./components/ErrorPage";
import { useState } from "react";


function routerFunction() {
  
const [error, setError] = useState("");

async function countryLoader({ params }) {
  try {
    const data = await fetch(
      `https://restcountries.com/v3.1/alpha/${params.id}`
    );
    const response = await data.json();

    if(response.message){
      console.log(response.message);
      setError(response.message);
      throw new Error(response.message) ;
    }
    return response;

  } catch (error) {
    console.log(error.message);
    throw new Error(error.message);
  }
}

const router = createBrowserRouter([
  { path: "/", element: <Countries /> },
  {
    path: "country/:id",
    loader: countryLoader,
    element: <Details />,
    errorElement: <ErrorPage error={error}/>,
  },
]);

return router
}

export default routerFunction;
